module eu.ngong.m2e11j5 {
	exports eu.ngong.m2e11j5;

	requires org.apache.commons.io;

	/*
	 * "opens <the module>" is required if test shall work with "mvn test" from the
	 * commandline or "Run As / Maven test" from Eclipse Package Explorer. It is not
	 * required from Eclipse "Run As / JUnit Test"
	 */
	opens eu.ngong.m2e11j5;
}
