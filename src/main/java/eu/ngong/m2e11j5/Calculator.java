package eu.ngong.m2e11j5;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import org.apache.commons.io.FileUtils;

public class Calculator {

	public int add(int a, int b) throws IOException {

		/* in tmp-dir create a specific file for that calculation and write the equation to it */
		FileUtils.write(new File(String.format("%s%sresult%dplus%d.txt", System.getProperty("java.io.tmpdir"),
				System.getProperty("file.separator"), a, b)),String.format("%d + %d = %d", a, b, a+b), Charset.defaultCharset());
		
		return a + b;
	}
}
