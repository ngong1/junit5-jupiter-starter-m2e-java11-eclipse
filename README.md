I pushed this example of using Eclipse with m2e for creating and testing a Java 11 module according to a [stackoverflow discussion](https://stackoverflow.com/questions/52804620/eclipse-m2e-junit5-already-possible/52806560?noredirect=1#comment92569156_52806560)

This example is based on [junit5-jupiter-starter-maven](https://github.com/junit-team/junit5-samples/tree/master/junit5-jupiter-starter-maven).

If you find that Eclipse does not use the dependencies laid down in pom.xml,
try to right-click the project in Package Explorer and select *Maven / Update Project ...*

**What I did to create this example**:
 - Had Eclipse 2018-09 ready with m2e and JDT Patch with Java 11 support
 - In Eclipse created a maven project from *simple project* archetype
 - changed *pom.xml* to use **maven-compiler-plugin 3.8.0** and **maven-surefire-plugin 2.22.1**
    - set source and target of the comiler plugin to 11
    - made it dependent on *junit-jupiter-api*, *junit-jupiter-params*, and *junit-jupiter-engine*
 - checked the Build Path (right-click *Configure Build Path ...* on the project in Package Explorer)
     - the *compiler comliance level* shall expose **11**
     - *Modulepath* shall host **JRE System Library [jdk-11]**
 - created a new package in *src/main/java* (this becomes the so-called *named module*) with a new class (*Calculator.java* in this case)
 - created a new file (not a class) named *module-info.java* in *src/main/java*
     - entered the name of the package as the module name
     - entered the *exports* line for that module (not needed if there is no other java module requiring it).
     - entered *requires* lines for modules needed by the app, but no junit5 module names
     - entered **opens <module name>**
         - needed for *Run As / Maven test* or *mvn test* from the commandline
         - not needed if you only use *Run As / JUnit* for testing
 - created new package and class for testing in *src/test/java*
 
**Important**: right-click the project in Package Explorer an select **Maven / Update Project ...**